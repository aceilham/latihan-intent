package com.ace.ch3top2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ace.ch3top2.data.Person1
import com.ace.ch3top2.data.Person2
import com.ace.ch3top2.databinding.ActivityStartBinding

class ActivityStart : AppCompatActivity() {

    private lateinit var binding: ActivityStartBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val fullName = binding.etNama.text
        val nickname = binding.etPanggilan.text
        val age = binding.etUsia.text
        val address = binding.etAlamat.text

        binding.satuan.setOnClickListener{
            val intent = Intent(this,ActivityEnd::class.java)
            intent.putExtra("SATUAN_NAMA", fullName.toString())
            intent.putExtra("SATUAN_PANGGILAN", nickname.toString())
            intent.putExtra("SATUAN_USIA", age.toString().toInt())
            intent.putExtra("SATUAN_ALAMAT", address.toString())

            startActivity(intent)
        }
        binding.bundle.setOnClickListener{
            val intent = Intent(this,ActivityEnd::class.java)
            val bundle = Bundle()
            bundle.putString("BUNDLE_NAMA", fullName.toString())
            bundle.putString("BUNDLE_PANGGILAN", nickname.toString())
            bundle.putString("BUNDLE_USIA", age.toString())
            bundle.putString("BUNDLE_ALAMAT", address.toString())

            intent.putExtras(bundle)
            startActivity(intent)
        }
        binding.serializable.setOnClickListener{
            val intent = Intent(this,ActivityEnd::class.java)
            val person = Person1(
                fullName.toString(),
                nickname.toString(),
                age.toString(),
                address.toString()
            )
            intent.putExtra("SERIALIZABLE", person)
            startActivity(intent)
        }
        binding.parcelable.setOnClickListener{
            val intent = Intent(this,ActivityEnd::class.java)
            val person = Person2(
                fullName.toString(),
                nickname.toString(),
                age.toString(),
                address.toString()
            )
            intent.putExtra("PARCELABLE", person)
            startActivity(intent)
        }
    }
}
