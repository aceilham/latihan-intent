package com.ace.ch3top2.data

import java.io.Serializable

data class Person1(
    val name: String?,
    val nick: String?,
    val age: String,
    val address: String?
) : Serializable