package com.ace.ch3top2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.ace.ch3top2.data.Person1
import com.ace.ch3top2.databinding.ActivityEndBinding
import com.ace.ch3top2.data.Person2

class ActivityEnd : AppCompatActivity() {

    private lateinit var binding: ActivityEndBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityEndBinding.inflate(layoutInflater)
        setContentView(binding.root)

        intent.getStringExtra("SATUAN_NAMA")?.let {
            val name = intent.getStringExtra("SATUAN_NAMA")
            val nick = intent.getStringExtra("SATUAN_PANGGILAN")
            val age = intent.getIntExtra("SATUAN_USIA", 0)
            val address = intent.getStringExtra("SATUAN_ALAMAT")
            binding.textV.text = "Halo $name, $nick, umur $age, beralamat di $address!"
        }
        intent.getStringExtra("BUNDLE_NAMA")?.let {
            val name = intent.getStringExtra("BUNDLE_NAMA")
            val nick = intent.getStringExtra("BUNDLE_PANGGILAN")
            val age = intent.getStringExtra("BUNDLE_USIA")
            val address = intent.getStringExtra("BUNDLE_ALAMAT")
            binding.textV.text = "Halo $name, $nick, umur $age, beralamat di $address"
        }
        intent.getSerializableExtra("SERIALIZABLE")?.let {
            val person = intent.getSerializableExtra("SERIALIZABLE") as Person1
            binding.textV.text = "Halo ${person.name}, ${person.nick}, umur ${person.age}, beralamat di ${person.address}"
        }
        intent.getParcelableExtra<Person2>("PARCELABLE")?.let {
            val person = intent.getParcelableExtra<Person2>("PARCELABLE")
            binding.textV.text = "Halo ${person?.name}, ${person?.nick}, umur ${person?.age}, beralamat di ${person?.address}"
        }
    }



}